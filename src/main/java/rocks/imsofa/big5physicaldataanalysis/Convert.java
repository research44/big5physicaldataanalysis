/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.big5physicaldataanalysis;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

/**
 *
 * @author lendle
 */
public class Convert {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        //File dateRoot=new File("20210430");
//        File dateRoot = new File("20210515");
        File dateRoot = new File("20211020");
        String[] groups = new String[]{"experiment", "control", "full"};
//        String[] groups = new String[]{"full"};
        for (String groupName : groups) {
            File root=new File(dateRoot, groupName);
            File sourceRoot = new File(root, "all data");
            List<Record> hrData = new ArrayList<>();
            List<Record> gsrData = new ArrayList<>();
            List<Record> frequencyData = new ArrayList<>();
            List<Record> volumeData = new ArrayList<>();
            for (File personalFile : sourceRoot.listFiles()) {
                String id = personalFile.getName();
                for (File csvFile : personalFile.listFiles()) {
                    if (csvFile.isDirectory()) {
                        continue;
                    }
                    List<Record> personalList=new ArrayList<>();
                    List<Record> currentList = null;
                    if (csvFile.getName().equals("volumes.csv")) {
                        currentList = volumeData;
                    } else if (csvFile.getName().equals("hr.csv")) {
                        currentList = hrData;
                    } else if (csvFile.getName().equals("frequencies.csv")) {
                        currentList = frequencyData;
                    } else {
                        currentList = gsrData;
                    }
                    long startTime = -1;
                    long lastTimestamp = -1;
                    try (FileReader reader = new FileReader(csvFile)) {
                        Iterable<CSVRecord> records = CSVFormat.EXCEL.withHeader().parse(reader);
                        for (CSVRecord cSVRecord : records) {
                            Record record = new Record();
                            if (startTime == -1) {
                                startTime = toTimestamp(cSVRecord.get("timestamp"));
                                record.setTimestamp(0);
                            } else {
                                long timestamp = toTimestamp(cSVRecord.get("timestamp"));
                                if (timestamp == lastTimestamp) {
                                    continue;
                                }
                                record.setTimestamp(timestamp - startTime);
                            }
                            lastTimestamp = toTimestamp(cSVRecord.get("timestamp"));
                            record.setId(id);
                            record.setValue(Double.valueOf(cSVRecord.get("value")));
                            personalList.add(record);
                        }
                    }
                    for(Record record : personalList){
                        record.setFinishTimestamp(lastTimestamp-startTime);
                    }
                    currentList.addAll(personalList);
                }
            }
            File physicaldata_processed = new File(root, "physicaldata_processed");
            physicaldata_processed.mkdir();
            writeCSV(gsrData, new File(physicaldata_processed, "gsr.csv"));
            writeCSV(hrData, new File(physicaldata_processed, "hr.csv"));
            writeCSV(frequencyData, new File(physicaldata_processed, "frequency.csv"));
            writeCSV(volumeData, new File(physicaldata_processed, "volume.csv"));
        }
    }
    
    static long toTimestamp(String str){
        str=str.toUpperCase();
        if(str.contains("E")){
            str=str.substring(0, str.indexOf("E"));
            str=str.replace(".", "");
            return Long.valueOf(str);
        }else{
            return Long.valueOf(str);
        }
    }
    
    static void writeCSV(List<Record> records, File file) throws Exception {
        try (PrintWriter writer = new PrintWriter(new FileWriter(file))) {
            writer.println("id,timestamp,value,lastTimestamp");
            for (Record record : records) {
                writer.println("\"" + record.getId() + "\"," + record.getTimestamp() + "," + record.getValue()+","+record.getFinishTimestamp());
            }
        }
    }

}
