/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.big5physicaldataanalysis;

import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.math.stat.descriptive.DescriptiveStatistics;

/**
 *
 * @author lendle
 */
public class Transform {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        //File dateRoot=new File("20210430");
//        File dateRoot = new File("20210515");
        File dateRoot = new File("20211020");
        String[] groups = new String[]{"experiment", "control", "full"};
//        String[] groups = new String[]{"full"};
        for (String groupName : groups) {
            File root1=new File(dateRoot, groupName);
            File root = new File(root1, "physicaldata_processed");
            for (File csvFile : root.listFiles()) {
                if (csvFile.getName().startsWith("transformed_")) {
                    continue;
                }
                if (!(csvFile.getName().equals("hr.csv") || csvFile.getName().equals("gsr.csv") || csvFile.getName().equals("frequency.csv"))) {
                    continue;
                }
                long thresholdTimestamp=-1;
                File transformedFile = new File(root, "transformed_" + csvFile.getName());
                try (FileReader reader = new FileReader(csvFile); PrintWriter writer = new PrintWriter(transformedFile)) {
                    writer.println("id,timestamp,mean,sd,skew");
                    Iterable<CSVRecord> records = CSVFormat.EXCEL.withHeader().parse(reader);
                    List<Double> values = new ArrayList<>();
//                    final long FIRST_TIME_LIMIT=30000;
//                    final long SAMPLE_TIME_INTERVAL=30000;
                    final long FIRST_TIME_LIMIT=15000;
                    final long SAMPLE_TIME_INTERVAL=15000;
                    long timeLimit = FIRST_TIME_LIMIT;
                    String currentId = null;
                    for (CSVRecord record : records) {
                        long timestamp = Long.valueOf(record.get("timestamp"));
                        long finishTimestamp=Long.valueOf(record.get("lastTimestamp"));
                        if(csvFile.getName().equals("frequency.csv")){
                            thresholdTimestamp=finishTimestamp-60000;
                        }else{
                            thresholdTimestamp=-1;
                        }
                        double value = Double.valueOf(record.get("value"));
                        if (timestamp == 0) {
                            //next id
                            if (values.isEmpty() == false) {
                                //output existing data
                                outputTransformedValues(csvFile, writer, currentId, timeLimit - SAMPLE_TIME_INTERVAL, thresholdTimestamp, values);
                                values.clear();
                            }
                            timeLimit = FIRST_TIME_LIMIT;
                            values.add(value);
                            currentId = record.get("id");
                        } else if (timestamp > timeLimit) {
                            //next segment
                            if (values.isEmpty() == false) {
                                //output existing data
                                outputTransformedValues(csvFile, writer, currentId, timeLimit - SAMPLE_TIME_INTERVAL, thresholdTimestamp, values);
                                values.clear();
                            }
                            values.add(value);
                            timeLimit += SAMPLE_TIME_INTERVAL;
                        } else {
                            values.add(value);
                        }
                    }
                    if (values.isEmpty() == false) {
                        //output existing data
                        outputTransformedValues(csvFile, writer, currentId, timeLimit - SAMPLE_TIME_INTERVAL, thresholdTimestamp, values);
                        values.clear();
                    }
                }
            }
        }
    }
    /**
     * 
     * @param sourceCSVFile
     * @param output
     * @param currentId
     * @param timestamp
     * @param thresholdTimestamp the min of timestamp to include this record
     * @param values 
     */
    private static void outputTransformedValues(File sourceCSVFile, PrintWriter output, String currentId, long timestamp, 
            long thresholdTimestamp, List<Double> values) {
        if(timestamp<thresholdTimestamp){
            return;
        }
        DescriptiveStatistics stats = new DescriptiveStatistics(values.stream().mapToDouble(Double::doubleValue).toArray());
        output.println("\"" + currentId + "\"," + timestamp + "," + stats.getMean() + ","
                + stats.getStandardDeviation() + "," + stats.getSkewness());
        if (stats.getMean() == 0) {
            System.out.println(values.size() + ":" + stats.getSkewness());
        }
    }
}
