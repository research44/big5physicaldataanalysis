/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.big5physicaldataanalysis;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

/**
 * Due to an error in signal collecting module,
 * the names of volumes and frequencies data have been
 * wrongly inchanged.
 * @author lendle
 */
public class FixWrongFileName {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        //File dateRoot=new File("20210430");
        File dateRoot = new File("20211020");
        String[] groups = new String[]{"experiment", "control", "full"};
        for (String groupName : groups) {
            File root=new File(dateRoot, groupName);
            File sourceRoot = new File(root, "all data");
            
            for (File personalFile : sourceRoot.listFiles()) {
                File volumesData=new File(personalFile, "volumes.csv");
                File frequenciesData=new File(personalFile, "frequencies.csv");
               
                if(volumesData.length()>frequenciesData.length()){
                    //then interchange the two files
                    File tempFile=new File(personalFile, "temp.csv");
                    FileUtils.copyFile(volumesData, tempFile);
                    FileUtils.copyFile(frequenciesData, volumesData);
                    FileUtils.copyFile(tempFile, frequenciesData);
                    FileUtils.deleteQuietly(tempFile);
                }
            }
        }
    }


}
